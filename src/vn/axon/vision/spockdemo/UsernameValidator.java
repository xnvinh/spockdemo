package vn.axon.vision.spockdemo;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.apache.commons.lang3.StringUtils;

@FacesValidator("usernameValidator")
public class UsernameValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		String username = (String) value;
		if (StringUtils.isBlank(username)) {
			FacesMessage message = new FacesMessage("Please input username.");
			context.addMessage(component.getClientId(), message);
		} else if (username.matches(".*[!@#\\$% ]+.*")) {
			FacesMessage message = new FacesMessage("Username should not contain !@#$% and SPACE.");
			context.addMessage(component.getClientId(), message);
		} else if (username.length() > 20) {
			FacesMessage message = new FacesMessage("Username should not longer than 20 characters.");
			context.addMessage(component.getClientId(), message);
		}
	}

}
