package vn.axon.vision.spockdemo.bank;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

public class BankAccountDataSource extends ListDataModel<BankAccount> implements SelectableDataModel<BankAccount> {
	@SuppressWarnings("unchecked")
	public List<BankAccount> getAccounts() {
		if (getWrappedData() == null) {
			setWrappedData(new ArrayList<BankAccount>());
		}
		return (List<BankAccount>) getWrappedData();
	}

	@Override
	public Object getRowKey(BankAccount account) {
		return account.getName();
	}

	@Override
	public BankAccount getRowData(String rowKey) {
		return getAccounts().stream().filter(account -> account.getName().equals(rowKey)).findFirst().orElse(null);
	}
}
