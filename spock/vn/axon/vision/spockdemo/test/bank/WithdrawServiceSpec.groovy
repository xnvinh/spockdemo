package vn.axon.vision.spockdemo.test.bank

import spock.lang.Specification
import spock.lang.Unroll
import vn.axon.vision.spockdemo.bank.AccountType
import vn.axon.vision.spockdemo.bank.BankAccount
import vn.axon.vision.spockdemo.bank.WithdrawService

class WithdrawServiceSpec extends Specification {
	@Unroll //("Customer #inputName withdraws #withdrawAmount from #inputAccountType account. Input balance: #inputBalance, remaining balance #remainingBalance")
	def "should calculate transaction fee base on AccountType"() {
		given:
			BankAccount account = BankAccount.create(inputName, inputBalance, inputAccountType);
			WithdrawService withdrawService = WithdrawService.create(account);
		when:
			withdrawService.withdraw(withdrawAmount);
		then:
			remainingBalance == account.balance;
		where:
			inputName				| inputBalance		| inputAccountType		| withdrawAmount	|| remainingBalance
			"Nguyen Xuan Vinh"		| 1000.0			| AccountType.STANDARD	| 500.0				|| 400.0
			"Bui Thi Huong Lan"		| 1000.0			| AccountType.VIP		| 500.0				|| 500.0
//			"Quach Tinh"			| 1000.0			| AccountType.MOBILE	| 500.0				|| 500.0
	}
}