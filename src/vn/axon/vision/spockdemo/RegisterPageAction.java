package vn.axon.vision.spockdemo;

import ch.ivyteam.ivy.environment.Ivy;
import vn.axon.vision.spockdemo.RegisterPage.RegisterPageData;

public class RegisterPageAction {
	private RegisterPageData pageData;
	
	private RegisterPageAction(RegisterPageData pageData) {
		this.pageData = pageData;
	}
	
	public static RegisterPageAction of(RegisterPageData pageData) {
		return new RegisterPageAction(pageData);
	}
	
	public void onSubmit() {
		Ivy.log().warn("Form submited");
	}
}
