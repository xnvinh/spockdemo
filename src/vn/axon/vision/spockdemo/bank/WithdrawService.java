package vn.axon.vision.spockdemo.bank;

public class WithdrawService {
	private BankAccount account;
	
	private WithdrawService(BankAccount account) {
		this.account = account;
	}
	
	public static WithdrawService create(BankAccount account) {
		return new WithdrawService(account);
	}
	
	public double withdraw(double amount) {
		if (account.getType() == AccountType.STANDARD) {
			double remainingBalance = account.getBalance() - amount - 100.0;
			account.setBalance(remainingBalance);
			return account.getBalance();
		} else {
			double remainingBalance = account.getBalance() - amount;
			account.setBalance(remainingBalance);
			return account.getBalance();
		}
		
		/*if (account.getType() == AccountType.STANDARD) {
				double remainingBalance = account.getBalance() - amount - 150.0;
				account.setBalance(remainingBalance);
				return account.getBalance();
		} else if(account.getType() == AccountType.MOBILE) {
			double remainingBalance = account.getBalance() - amount - 200.0;
			account.setBalance(remainingBalance);
			return account.getBalance();
		} else {
			double remainingBalance = account.getBalance() - amount;
			account.setBalance(remainingBalance);
			return account.getBalance();
		}*/
	}
}
