[Ivy]
[>Created: Mon Jan 15 19:21:37 ICT 2018]
15F9C9B04FB82F6A 3.18 #module
>Proto >Proto Collection #zClass
Rr0 Register Big #zClass
Rr0 B #cInfo
Rr0 #process
Rr0 @TextInP .resExport .resExport #zField
Rr0 @TextInP .type .type #zField
Rr0 @TextInP .processKind .processKind #zField
Rr0 @AnnotationInP-0n ai ai #zField
Rr0 @MessageFlowInP-0n messageIn messageIn #zField
Rr0 @MessageFlowOutP-0n messageOut messageOut #zField
Rr0 @TextInP .xml .xml #zField
Rr0 @TextInP .responsibility .responsibility #zField
Rr0 @StartRequest f0 '' #zField
Rr0 @EndTask f1 '' #zField
Rr0 @RichDialog f3 '' #zField
Rr0 @PushWFArc f4 '' #zField
Rr0 @PushWFArc f2 '' #zField
>Proto Rr0 Rr0 Register #zField
Rr0 f0 outLink start.ivp #txt
Rr0 f0 type vn.axon.vision.spockdemo.RegisterData #txt
Rr0 f0 inParamDecl '<> param;' #txt
Rr0 f0 actionDecl 'vn.axon.vision.spockdemo.RegisterData out;
' #txt
Rr0 f0 guid 15F9C9B06504FEB1 #txt
Rr0 f0 requestEnabled true #txt
Rr0 f0 triggerEnabled false #txt
Rr0 f0 callSignature start() #txt
Rr0 f0 persist false #txt
Rr0 f0 taskData 'TaskTriggered.ROL=Everybody
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.EXROL=Everybody' #txt
Rr0 f0 caseData businessCase.attach=true #txt
Rr0 f0 showInStartList 0 #txt
Rr0 f0 taskAndCaseSetupAction 'import ch.ivyteam.ivy.workflow.TaskUpdateDefinition;
ch.ivyteam.ivy.workflow.TaskUpdateDefinition taskUpdDef = new ch.ivyteam.ivy.workflow.TaskUpdateDefinition();
import ch.ivyteam.ivy.request.impl.DefaultCalendarProxy;
DefaultCalendarProxy calendarProxy = ivy.cal as DefaultCalendarProxy;
taskUpdDef.setPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
taskUpdDef.setExpiryActivator("Everybody");
taskUpdDef.setExpiryPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
engine.updateCurrentTask(taskUpdDef);
' #txt
Rr0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
        <nameStyle>9,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Rr0 f0 @C|.responsibility Everybody #txt
Rr0 f0 81 49 30 30 -25 17 #rect
Rr0 f0 @|StartRequestIcon #fIcon
Rr0 f1 type vn.axon.vision.spockdemo.RegisterData #txt
Rr0 f1 337 49 30 30 0 15 #rect
Rr0 f1 @|EndIcon #fIcon
Rr0 f3 targetWindow NEW:card: #txt
Rr0 f3 targetDisplay TOP #txt
Rr0 f3 richDialogId vn.axon.vision.spockdemo.RegisterPage #txt
Rr0 f3 startMethod start() #txt
Rr0 f3 type vn.axon.vision.spockdemo.RegisterData #txt
Rr0 f3 requestActionDecl '<> param;' #txt
Rr0 f3 responseActionDecl 'vn.axon.vision.spockdemo.RegisterData out;
' #txt
Rr0 f3 responseMappingAction 'out=in;
' #txt
Rr0 f3 windowConfiguration '* ' #txt
Rr0 f3 isAsynch false #txt
Rr0 f3 isInnerRd false #txt
Rr0 f3 userContext '* ' #txt
Rr0 f3 168 42 112 44 0 -8 #rect
Rr0 f3 @|RichDialogIcon #fIcon
Rr0 f4 expr out #txt
Rr0 f4 111 64 168 64 #arcP
Rr0 f2 expr out #txt
Rr0 f2 280 64 337 64 #arcP
>Proto Rr0 .type vn.axon.vision.spockdemo.RegisterData #txt
>Proto Rr0 .processKind NORMAL #txt
>Proto Rr0 0 0 32 24 18 0 #rect
>Proto Rr0 @|BIcon #fIcon
Rr0 f0 mainOut f4 tail #connect
Rr0 f4 head f3 mainIn #connect
Rr0 f3 mainOut f2 tail #connect
Rr0 f2 head f1 mainIn #connect
