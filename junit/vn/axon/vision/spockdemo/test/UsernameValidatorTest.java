package vn.axon.vision.spockdemo.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import vn.axon.vision.spockdemo.UsernameValidator;

public class UsernameValidatorTest {
	private FacesContext facesContext;
	private UIComponent uiComponent;
	
	private UsernameValidator validator;
	
	@Before
	public void setup() {
		facesContext = mock(FacesContext.class);
		uiComponent = mock(UIComponent.class);
		validator = new UsernameValidator();
	}

	@Test
	public void should_show_special_character_message_1() {
		when(uiComponent.getClientId()).thenReturn("username");
		ArgumentCaptor<FacesMessage> messageCaptor = ArgumentCaptor.forClass(FacesMessage.class);
		
		validator.validate(facesContext, uiComponent, "vision#");		
		verify(facesContext).addMessage(eq("username"), messageCaptor.capture());
		assertEquals("Username should not contain !@#$% and SPACE.", messageCaptor.getValue().getSummary());
	}
	
	@Test
	public void should_show_special_character_message_2() {
		when(uiComponent.getClientId()).thenReturn("username");
		ArgumentCaptor<FacesMessage> messageCaptor = ArgumentCaptor.forClass(FacesMessage.class);
		
		validator.validate(facesContext, uiComponent, "vision%");		
		verify(facesContext).addMessage(eq("username"), messageCaptor.capture());
		assertEquals("Username should not contain !@#$% and SPACE.", messageCaptor.getValue().getSummary());
	}
	
	@Test
	public void should_show_special_character_message_3() {
		when(uiComponent.getClientId()).thenReturn("username");
		ArgumentCaptor<FacesMessage> messageCaptor = ArgumentCaptor.forClass(FacesMessage.class);
		
		validator.validate(facesContext, uiComponent, "visi@n");		
		verify(facesContext).addMessage(eq("username"), messageCaptor.capture());
		assertEquals("Username should not contain !@#$% and SPACE.", messageCaptor.getValue().getSummary());
	}
}
