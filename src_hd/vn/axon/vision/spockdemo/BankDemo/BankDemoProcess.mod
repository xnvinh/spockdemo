[Ivy]
[>Created: Mon Jan 15 20:17:32 ICT 2018]
160F9AF73F8135C3 3.18 #module
>Proto >Proto Collection #zClass
Bs0 BankDemoProcess Big #zClass
Bs0 RD #cInfo
Bs0 #process
Bs0 @TextInP .ui2RdDataAction .ui2RdDataAction #zField
Bs0 @TextInP .rdData2UIAction .rdData2UIAction #zField
Bs0 @TextInP .resExport .resExport #zField
Bs0 @TextInP .type .type #zField
Bs0 @TextInP .processKind .processKind #zField
Bs0 @AnnotationInP-0n ai ai #zField
Bs0 @MessageFlowInP-0n messageIn messageIn #zField
Bs0 @MessageFlowOutP-0n messageOut messageOut #zField
Bs0 @TextInP .xml .xml #zField
Bs0 @TextInP .responsibility .responsibility #zField
Bs0 @RichDialogInitStart f0 '' #zField
Bs0 @RichDialogProcessEnd f1 '' #zField
Bs0 @PushWFArc f2 '' #zField
Bs0 @RichDialogProcessStart f3 '' #zField
Bs0 @RichDialogEnd f4 '' #zField
Bs0 @PushWFArc f5 '' #zField
Bs0 @RichDialogMethodStart f6 '' #zField
Bs0 @RichDialogProcessEnd f7 '' #zField
Bs0 @PushWFArc f8 '' #zField
Bs0 @RichDialogProcessStart f9 '' #zField
Bs0 @RichDialogProcessEnd f10 '' #zField
Bs0 @PushWFArc f11 '' #zField
>Proto Bs0 Bs0 BankDemoProcess #zField
Bs0 f0 guid 160F9AF7418F433A #txt
Bs0 f0 type vn.axon.vision.spockdemo.BankDemo.BankDemoData #txt
Bs0 f0 method start() #txt
Bs0 f0 disableUIEvents true #txt
Bs0 f0 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Bs0 f0 inActionCode 'import vn.axon.vision.spockdemo.bank.BankAccountDataSource;
import vn.axon.vision.spockdemo.bank.AccountType;
import vn.axon.vision.spockdemo.bank.BankAccount;
out.accounts = new BankAccountDataSource();

out.accounts.getAccounts().add(BankAccount.create("Nguyen Xuan Vinh", 1000, AccountType.STANDARD));
out.accounts.getAccounts().add(BankAccount.create("Do Viet Trung", 1000, AccountType.STANDARD));
out.accounts.getAccounts().add(BankAccount.create("Le Chi Cuong", 1000, AccountType.STANDARD));
out.accounts.getAccounts().add(BankAccount.create("Nguyen Hai Dang", 1000, AccountType.STANDARD));
out.accounts.getAccounts().add(BankAccount.create("Bui Thi Huong Lan", 1000000000, AccountType.VIP));
' #txt
Bs0 f0 outParameterDecl '<> result;
' #txt
Bs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start()</name>
        <nameStyle>7,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Bs0 f0 83 51 26 26 -20 15 #rect
Bs0 f0 @|RichDialogInitStartIcon #fIcon
Bs0 f1 type vn.axon.vision.spockdemo.BankDemo.BankDemoData #txt
Bs0 f1 211 51 26 26 0 12 #rect
Bs0 f1 @|RichDialogProcessEndIcon #fIcon
Bs0 f2 expr out #txt
Bs0 f2 109 64 211 64 #arcP
Bs0 f3 guid 160F9AF7427BC1B1 #txt
Bs0 f3 type vn.axon.vision.spockdemo.BankDemo.BankDemoData #txt
Bs0 f3 actionDecl 'vn.axon.vision.spockdemo.BankDemo.BankDemoData out;
' #txt
Bs0 f3 actionTable 'out=in;
' #txt
Bs0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
    </language>
</elementInfo>
' #txt
Bs0 f3 83 147 26 26 -16 12 #rect
Bs0 f3 @|RichDialogProcessStartIcon #fIcon
Bs0 f4 type vn.axon.vision.spockdemo.BankDemo.BankDemoData #txt
Bs0 f4 guid 160F9AF742742733 #txt
Bs0 f4 211 147 26 26 0 12 #rect
Bs0 f4 @|RichDialogEndIcon #fIcon
Bs0 f5 expr out #txt
Bs0 f5 109 160 211 160 #arcP
Bs0 f6 guid 160F9D957422526D #txt
Bs0 f6 type vn.axon.vision.spockdemo.BankDemo.BankDemoData #txt
Bs0 f6 method onAccountSelected(org.primefaces.event.SelectEvent) #txt
Bs0 f6 disableUIEvents false #txt
Bs0 f6 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<org.primefaces.event.SelectEvent event> param = methodEvent.getInputArguments();
' #txt
Bs0 f6 inActionCode 'import vn.axon.vision.spockdemo.bank.BankAccount;
out.selectedAccount = param.event.getObject() as BankAccount;' #txt
Bs0 f6 outParameterDecl '<> result;
' #txt
Bs0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>onAccountSelected(SelectEvent)</name>
        <nameStyle>30,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Bs0 f6 211 307 26 26 -103 15 #rect
Bs0 f6 @|RichDialogMethodStartIcon #fIcon
Bs0 f7 type vn.axon.vision.spockdemo.BankDemo.BankDemoData #txt
Bs0 f7 403 307 26 26 0 12 #rect
Bs0 f7 @|RichDialogProcessEndIcon #fIcon
Bs0 f8 expr out #txt
Bs0 f8 237 320 403 320 #arcP
Bs0 f9 guid 160F9EC11D082811 #txt
Bs0 f9 type vn.axon.vision.spockdemo.BankDemo.BankDemoData #txt
Bs0 f9 actionDecl 'vn.axon.vision.spockdemo.BankDemo.BankDemoData out;
' #txt
Bs0 f9 actionTable 'out=in;
' #txt
Bs0 f9 actionCode 'import vn.axon.vision.spockdemo.bank.WithdrawService;
WithdrawService withdrawService = WithdrawService.create(out.selectedAccount);
withdrawService.withdraw(out.withdrawAmount);
out.selectedAccount = null;
out.withdrawAmount = null;' #txt
Bs0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>processWithdraw</name>
        <nameStyle>15,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Bs0 f9 211 435 26 26 -53 15 #rect
Bs0 f9 @|RichDialogProcessStartIcon #fIcon
Bs0 f10 type vn.axon.vision.spockdemo.BankDemo.BankDemoData #txt
Bs0 f10 531 435 26 26 0 12 #rect
Bs0 f10 @|RichDialogProcessEndIcon #fIcon
Bs0 f11 expr out #txt
Bs0 f11 237 448 531 448 #arcP
>Proto Bs0 .type vn.axon.vision.spockdemo.BankDemo.BankDemoData #txt
>Proto Bs0 .processKind HTML_DIALOG #txt
>Proto Bs0 -8 -8 16 16 16 26 #rect
>Proto Bs0 '' #fIcon
Bs0 f0 mainOut f2 tail #connect
Bs0 f2 head f1 mainIn #connect
Bs0 f3 mainOut f5 tail #connect
Bs0 f5 head f4 mainIn #connect
Bs0 f6 mainOut f8 tail #connect
Bs0 f8 head f7 mainIn #connect
Bs0 f9 mainOut f11 tail #connect
Bs0 f11 head f10 mainIn #connect
