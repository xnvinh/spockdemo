package vn.axon.vision.spockdemo.test


import static org.mockito.Mockito.*

import javax.faces.application.FacesMessage
import javax.faces.component.UIComponent
import javax.faces.context.FacesContext

import org.mockito.ArgumentCaptor

import spock.lang.Specification
import spock.lang.Unroll
import vn.axon.vision.spockdemo.UsernameValidator

class UsernameValidatorSpec extends Specification {
	private FacesContext facesContext
	UIComponent uiComponent
	private UsernameValidator validator
	
	def setup() {
		facesContext = mock(FacesContext)
		uiComponent = mock(UIComponent)
		validator = new UsernameValidator()
	}
	
	@Unroll
	def "should show special character message"() {
		given:
			when(uiComponent.getClientId()).thenReturn("username")
			ArgumentCaptor<FacesMessage> messageCaptor = ArgumentCaptor.forClass(FacesMessage)
		when:
			validator.validate(facesContext, uiComponent, input)
		then:
			verify(facesContext).addMessage(eq("username"), messageCaptor.capture())
			"Username should not contain !@#\$% and SPACE." == messageCaptor.getValue().getSummary()
		where:
			input	<< ["vision#", "vision!", "visi@n"]
	}
}