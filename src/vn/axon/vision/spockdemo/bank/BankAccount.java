package vn.axon.vision.spockdemo.bank;

public class BankAccount {
	private String name;
	private double balance;
	private AccountType type;
	
	public BankAccount() {}

	private BankAccount(String name, double balance, AccountType type) {
		this.name = name;
		this.balance = balance;
		this.type = type;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public static BankAccount create(String name, double balance, AccountType type) {
		return new BankAccount(name, balance, type);
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public AccountType getType() {
		return type;
	}
	

	public void setType(AccountType type) {
		this.type = type;
	}
	
	
}
