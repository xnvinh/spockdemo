package vn.axon.vision.spockdemo.test.bank;

import static org.junit.Assert.*;

import org.junit.Test;

import vn.axon.vision.spockdemo.bank.AccountType;
import vn.axon.vision.spockdemo.bank.BankAccount;
import vn.axon.vision.spockdemo.bank.WithdrawService;

public class WithdrawServiceTest {
	@Test
	public void should_subtract_transaction_fee_for_STANDARD_account() {
		BankAccount account = BankAccount.create("Nguyen Xuan Vinh", 1000.0, AccountType.STANDARD);
		WithdrawService withdrawService = WithdrawService.create(account);
		
		assertEquals(400.0, withdrawService.withdraw(500.0), 0.001);
	}
	
	@Test
	public void should_free_transaction_for_VIP_account() {
		BankAccount account = BankAccount.create("Bui Thi Huong Lan", 1000.0, AccountType.VIP);
		WithdrawService withdrawService = WithdrawService.create(account);
		
		assertEquals(500.0, withdrawService.withdraw(500.0), 0.001);
	}
	
	/*@Test
	public void should_subtract_transaction_fee_for_MOBILE_account() throws Exception {
		BankAccount account = BankAccount.create("Quach Tinh", 1000.0, AccountType.MOBILE);
		WithdrawService withdrawService = WithdrawService.create(account);
		
		assertEquals(400.0, withdrawService.withdraw(500.0), 0.001);
	}*/
	
	/*@Test
	public void should_withdraw_base_on_account_type() throws Exception {
		BankAccount account = BankAccount.create("Nguyen Xuan Vinh", 1000.0, AccountType.STANDARD);
		WithdrawService withdrawService = WithdrawService.create(account);
		
		assertEquals(400.0, withdrawService.withdraw(500.0), 0.001);
		
		account = BankAccount.create("Bui Thi Huong Lan", 1000.0, AccountType.VIP);
		assertEquals(500.0, withdrawService.withdraw(500.0), 0.001);
		
		account = BankAccount.create("Quach Tinh", 1000.0, AccountType.MOBILE);
		assertEquals(400.0, withdrawService.withdraw(500.0), 0.001);
	}*/
}
