[Ivy]
15F9C9B92C2FE018 3.20 #module
>Proto >Proto Collection #zClass
Rs0 RegisterPageProcess Big #zClass
Rs0 RD #cInfo
Rs0 #process
Rs0 @TextInP .ui2RdDataAction .ui2RdDataAction #zField
Rs0 @TextInP .rdData2UIAction .rdData2UIAction #zField
Rs0 @TextInP .resExport .resExport #zField
Rs0 @TextInP .type .type #zField
Rs0 @TextInP .processKind .processKind #zField
Rs0 @AnnotationInP-0n ai ai #zField
Rs0 @MessageFlowInP-0n messageIn messageIn #zField
Rs0 @MessageFlowOutP-0n messageOut messageOut #zField
Rs0 @TextInP .xml .xml #zField
Rs0 @TextInP .responsibility .responsibility #zField
Rs0 @RichDialogInitStart f0 '' #zField
Rs0 @RichDialogProcessEnd f1 '' #zField
Rs0 @RichDialogProcessStart f3 '' #zField
Rs0 @RichDialogEnd f4 '' #zField
Rs0 @PushWFArc f5 '' #zField
Rs0 @RichDialogProcessStart f6 '' #zField
Rs0 @RichDialogProcessEnd f7 '' #zField
Rs0 @PushWFArc f8 '' #zField
Rs0 @GridStep f9 '' #zField
Rs0 @PushWFArc f10 '' #zField
Rs0 @PushWFArc f2 '' #zField
>Proto Rs0 Rs0 RegisterPageProcess #zField
Rs0 f0 guid 15F9C9B931029D6A #txt
Rs0 f0 type vn.axon.vision.spockdemo.RegisterPage.RegisterPageData #txt
Rs0 f0 method start() #txt
Rs0 f0 disableUIEvents true #txt
Rs0 f0 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Rs0 f0 outParameterDecl '<> result;
' #txt
Rs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start()</name>
    </language>
</elementInfo>
' #txt
Rs0 f0 83 51 26 26 -20 15 #rect
Rs0 f0 @|RichDialogInitStartIcon #fIcon
Rs0 f1 type vn.axon.vision.spockdemo.RegisterPage.RegisterPageData #txt
Rs0 f1 339 51 26 26 0 12 #rect
Rs0 f1 @|RichDialogProcessEndIcon #fIcon
Rs0 f3 guid 15F9C9B9335D6BDB #txt
Rs0 f3 type vn.axon.vision.spockdemo.RegisterPage.RegisterPageData #txt
Rs0 f3 actionDecl 'vn.axon.vision.spockdemo.RegisterPage.RegisterPageData out;
' #txt
Rs0 f3 actionTable 'out=in;
' #txt
Rs0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
    </language>
</elementInfo>
' #txt
Rs0 f3 83 147 26 26 -16 12 #rect
Rs0 f3 @|RichDialogProcessStartIcon #fIcon
Rs0 f4 type vn.axon.vision.spockdemo.RegisterPage.RegisterPageData #txt
Rs0 f4 guid 15F9C9B9335A8FA6 #txt
Rs0 f4 211 147 26 26 0 12 #rect
Rs0 f4 @|RichDialogEndIcon #fIcon
Rs0 f5 expr out #txt
Rs0 f5 109 160 211 160 #arcP
Rs0 f6 guid 15F9CAC524D88E45 #txt
Rs0 f6 type vn.axon.vision.spockdemo.RegisterPage.RegisterPageData #txt
Rs0 f6 actionDecl 'vn.axon.vision.spockdemo.RegisterPage.RegisterPageData out;
' #txt
Rs0 f6 actionTable 'out=in;
' #txt
Rs0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>submit</name>
        <nameStyle>6,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Rs0 f6 83 243 26 26 -21 15 #rect
Rs0 f6 @|RichDialogProcessStartIcon #fIcon
Rs0 f7 type vn.axon.vision.spockdemo.RegisterPage.RegisterPageData #txt
Rs0 f7 531 243 26 26 0 12 #rect
Rs0 f7 @|RichDialogProcessEndIcon #fIcon
Rs0 f8 expr out #txt
Rs0 f8 109 256 531 256 #arcP
Rs0 f9 actionDecl 'vn.axon.vision.spockdemo.RegisterPage.RegisterPageData out;
' #txt
Rs0 f9 actionTable 'out=in;
' #txt
Rs0 f9 actionCode 'import vn.axon.vision.spockdemo.Account;
in.temporaryAccount = new Account();' #txt
Rs0 f9 type vn.axon.vision.spockdemo.RegisterPage.RegisterPageData #txt
Rs0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>init temporary account</name>
        <nameStyle>22
</nameStyle>
    </language>
</elementInfo>
' #txt
Rs0 f9 144 42 160 44 -70 -8 #rect
Rs0 f9 @|StepIcon #fIcon
Rs0 f10 expr out #txt
Rs0 f10 109 64 144 64 #arcP
Rs0 f2 expr out #txt
Rs0 f2 304 64 339 64 #arcP
>Proto Rs0 .type vn.axon.vision.spockdemo.RegisterPage.RegisterPageData #txt
>Proto Rs0 .processKind HTML_DIALOG #txt
>Proto Rs0 -8 -8 16 16 16 26 #rect
>Proto Rs0 '' #fIcon
Rs0 f3 mainOut f5 tail #connect
Rs0 f5 head f4 mainIn #connect
Rs0 f6 mainOut f8 tail #connect
Rs0 f8 head f7 mainIn #connect
Rs0 f0 mainOut f10 tail #connect
Rs0 f10 head f9 mainIn #connect
Rs0 f9 mainOut f2 tail #connect
Rs0 f2 head f1 mainIn #connect
